#include <PS2X_lib.h>

#include "Declarations.h"
#include "MotorControl.h"

PS2X ps2x;


int error = 0;
byte type = 0;
byte vibrate = 0;



void setup() {
  Serial.begin(9600);
  initMotorDrivers();
  pinMode(bumper, OUTPUT);
  error = ps2x.config_gamepad(13, 11, 10, 12, true, true); //GamePad(clock, command, attention, data, Pressures?, Rumble?)

  if (error == 0) {
    Serial.println("Found Controller, configured successful");
    Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
    Serial.println("holding L1 or R1 will print out the analog stick values.");
    Serial.println("Go to www.billporter.info for updates and to report bugs.");
  }

  else if (error == 1)
    Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");

  else if (error == 2)
    Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");

  else if (error == 3)
    Serial.println("Controller refusing to enter Pressures mode, may not support it. ");

  type = ps2x.readType();
  switch (type) {
    case 0:
      Serial.println("Unknown Controller type");
      break;
    case 1:
      Serial.println("DualShock Controller Found");
      break;
    case 2:
      Serial.println("GuitarHero Controller Found");
      break;
  }


}

void loop() {
  ps2x.read_gamepad();
  //  Serial.print("right stick ");
  //  Serial.print(ps2x.Analog(PSS_RY));
  //  Serial.print(" left stick ");
  //  Serial.println( ps2x.Analog(PSS_LY));

  float right = map(ps2x.Analog(PSS_RY), 255, 0, -1, 1);
  float left = map(ps2x.Analog(PSS_LY), 255, 0, -1, 1);


  float mult; 

  if(ps2x.Button(PSB_R2) || ps2x.Button(PSB_L2)){
    mult = 255; 
  }else{
    mult = 127;
  }
  
  if(ps2x.Button(PSB_R1) || ps2x.Button(PSB_L1)){
   digitalWrite(bumper, HIGH); 
  }else{
    digitalWrite(bumper, LOW);
  }
  
  setRightMotors( (ps2x.Analog(PSS_RY) ^ 0b10000000) >> 7, (int)abs(right*mult));
  setLeftMotors( (ps2x.Analog(PSS_LY) ^ 0b10000000) >> 7, (int)abs(left*mult));


  delay(10);
}


