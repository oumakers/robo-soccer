
void initMotorDrivers() {
  pinMode(ren, OUTPUT);
  pinMode(rrpwm, OUTPUT);
  pinMode(rlpwm, OUTPUT);
  pinMode(len, OUTPUT);
  pinMode(lrpwm, OUTPUT);
  pinMode(llpwm, OUTPUT);
}

void setLeftMotors(byte dir, byte pwm) {
  if (pwm > 0) {
    digitalWrite(len, HIGH);
    switch (dir) {
      case forward:
        analogWrite(llpwm, pwm);
        analogWrite(lrpwm, 0);
        break;
      case backward:
        analogWrite(llpwm, 0);
        analogWrite(lrpwm, pwm);
        break;
    }
  }
  else {
    digitalWrite(len, LOW);
    analogWrite(llpwm, 0);
    analogWrite(lrpwm, 0);
  }
}


void setRightMotors(byte dir, byte pwm) {
  if (pwm > 0) {
    digitalWrite(ren, HIGH);
    switch (dir) {
      case forward:
        analogWrite(rlpwm, 0);
        analogWrite(rrpwm, pwm);
        break;
      case backward:
        analogWrite(rlpwm, pwm);
        analogWrite(rrpwm, 0);
        break;
    }
  }
  else {
    digitalWrite(ren, LOW);
    analogWrite(rlpwm, 0);
    analogWrite(rrpwm, 0);
  }
}
