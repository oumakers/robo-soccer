
#define ren 4
#define rrpwm 6
#define rlpwm 5

#define len 8
#define lrpwm 3
#define llpwm 9


#define forward 1
#define backward 0

void setup() {
  initMotorDrivers();

}

void loop() {

  for (int a = 0; a < 255; a++) {
    setRightMotors(forward, a);
    setLeftMotors(forward, a);
    delay(50);
  }

  for (int a = 0; a < 255; a++) {
    setRightMotors(backward, a);
    setLeftMotors(backward, a);
    delay(50);
  }

}

void initMotorDrivers() {
  pinMode(ren, OUTPUT);
  pinMode(rrpwm, OUTPUT);
  pinMode(rlpwm, OUTPUT);
  pinMode(len, OUTPUT);
  pinMode(lrpwm, OUTPUT);
  pinMode(llpwm, OUTPUT);
}

void setLeftMotors(byte dir, byte pwm) {
  if (pwm > 0) {
    digitalWrite(len, HIGH);
    switch (dir) {
      case forward:
        analogWrite(llpwm, pwm);
        analogWrite(lrpwm, 0);
        break;
      case backward:
        analogWrite(llpwm, 0);
        analogWrite(lrpwm, pwm);
        break;
    }
  }
  else {
    digitalWrite(len, LOW);
    analogWrite(llpwm, 0);
    analogWrite(lrpwm, 0);
  }
}


void setRightMotors(byte dir, byte pwm) {
  if (pwm > 0) {
    digitalWrite(ren, HIGH);
    switch (dir) {
      case forward:
        analogWrite(rlpwm, 0);
        analogWrite(rrpwm, pwm);
        break;
      case backward:
        analogWrite(rlpwm, pwm);
        analogWrite(rrpwm, 0);
        break;
    }
  }
  else {
    digitalWrite(ren, LOW);
    analogWrite(rlpwm, 0);
    analogWrite(rrpwm, 0);
  }
}

