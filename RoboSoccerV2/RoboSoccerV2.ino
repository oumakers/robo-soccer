#define F_CPU 16000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define rightSideEN 4 //PD3
#define rightSideDir 5 //PD4

#define leftSideEN 4 //PB3
#define leftSideDir 5 //PB4

byte x = 0;

int main(void) {

  //set PD3 and PB3 to outputs
  DDRB |= (1 << rightSideEN);
  DDRD |= (1 << leftSideEN);

  initPWM(); //initilize the pwm output compare units on timer 2

  OCR2A = 0;
  OCR2B = 0;

  while (true) {

    OCR2A++; 
    OCR2B++; 

    _delay_ms(100);

  }
}


void initPWM() {

  /*Configure TCCR2A to clear on match with fast PWM mode
     (see Atmega328 datasheet pg 203-205)
  */
  TCCR2A = 0b10100011;

  //configure Timer 2 Prescaler to 256 (see atmega328 datasheet pg 206-207)
  TCCR2B = 0b00000110;

}
