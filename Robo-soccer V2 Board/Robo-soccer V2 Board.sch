EESchema Schematic File Version 4
LIBS:Robo-soccer V2 Board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Robosoccer V2"
Date "2020-03-13"
Rev "1"
Comp "Makers@OU"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Robo-soccer-V2-Board-rescue:ESP32-WROOM-32-RF_Module U?
U 1 1 5E6BFC95
P 1900 2400
F 0 "U?" H 1150 4000 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 1450 3900 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 1900 900 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 1600 2450 50  0001 C CNN
F 4 "1904-1010-1-ND" H 1900 2400 50  0001 C CNN "digikey"
	1    1900 2400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E6C26FC
P 1900 1000
F 0 "#PWR?" H 1900 850 50  0001 C CNN
F 1 "+3.3V" H 1915 1173 50  0000 C CNN
F 2 "" H 1900 1000 50  0001 C CNN
F 3 "" H 1900 1000 50  0001 C CNN
	1    1900 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E6C3404
P 1900 3800
F 0 "#PWR?" H 1900 3550 50  0001 C CNN
F 1 "GND" H 1905 3627 50  0000 C CNN
F 2 "" H 1900 3800 50  0001 C CNN
F 3 "" H 1900 3800 50  0001 C CNN
	1    1900 3800
	1    0    0    -1  
$EndComp
Text Label 2900 1200 2    50   ~ 0
IO0
Text Label 1100 1200 0    50   ~ 0
EN
Wire Wire Line
	1300 1200 1100 1200
$Comp
L Device:R R?
U 1 1 5E6C1594
P 650 2550
F 0 "R?" H 720 2596 50  0000 L CNN
F 1 "10k" H 720 2505 50  0000 L CNN
F 2 "" V 580 2550 50  0001 C CNN
F 3 "~" H 650 2550 50  0001 C CNN
F 4 "311-10.0KCRCT-ND" H 650 2550 50  0001 C CNN "digikey"
	1    650  2550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E6C29BF
P 650 2200
F 0 "#PWR?" H 650 2050 50  0001 C CNN
F 1 "+3.3V" H 665 2373 50  0000 C CNN
F 2 "" H 650 2200 50  0001 C CNN
F 3 "" H 650 2200 50  0001 C CNN
	1    650  2200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E6C2FAF
P 950 2200
F 0 "#PWR?" H 950 2050 50  0001 C CNN
F 1 "+3.3V" H 965 2373 50  0000 C CNN
F 2 "" H 950 2200 50  0001 C CNN
F 3 "" H 950 2200 50  0001 C CNN
	1    950  2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  2200 650  2400
Wire Wire Line
	950  2400 950  2200
$Comp
L Device:R R?
U 1 1 5E6C4230
P 950 2550
F 0 "R?" H 1020 2596 50  0000 L CNN
F 1 "10k" H 1020 2505 50  0000 L CNN
F 2 "" V 880 2550 50  0001 C CNN
F 3 "~" H 950 2550 50  0001 C CNN
F 4 "311-10.0KCRCT-ND" H 950 2550 50  0001 C CNN "digikey"
	1    950  2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  2700 650  2950
Wire Wire Line
	950  2700 950  2950
Text Label 650  2950 1    50   ~ 0
IO0
Text Label 950  2950 1    50   ~ 0
EN
Text Notes 600  4600 0    50   ~ 0
Note: do not connect anything to IO0, IO2, IO12, or IO15
$Comp
L Robo-soccer-V2-Board-rescue:FT231XS-Interface_USB U?
U 1 1 5E6D1202
P 3100 6200
F 0 "U?" H 3100 7278 50  0000 C CNN
F 1 "FT231XS" H 3100 7187 50  0000 C CNN
F 2 "Package_SO:SSOP-20_3.9x8.7mm_P0.635mm" H 4100 5400 50  0001 C CNN
F 3 "https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT231X.pdf" H 3100 6200 50  0001 C CNN
F 4 "768-1129-1-ND" H 3100 6200 50  0001 C CNN "digikey"
	1    3100 6200
	1    0    0    -1  
$EndComp
Text Notes 6950 6650 0    50   ~ 0
Design Requirements: \n- 3 Amp motor drivers for 4 motors (may not be populated) \n- Space on board for robin-runcam and video transmitter) \n- Solenoid driver \n- space for bar-graph display in front of camera\n- modularity (fill any extra space on PCB with perf-board style holes)\n- Powered by 3-4S LIPO batteries (11.1V - 16.8V)\n- LED to indicate connection status\n\n\n
$Comp
L power:+3.3V #PWR?
U 1 1 5E6D7DDF
P 3000 5150
F 0 "#PWR?" H 3000 5000 50  0001 C CNN
F 1 "+3.3V" H 3015 5323 50  0000 C CNN
F 2 "" H 3000 5150 50  0001 C CNN
F 3 "" H 3000 5150 50  0001 C CNN
	1    3000 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5150 3000 5300
Wire Wire Line
	3200 5300 3200 4800
Wire Wire Line
	3200 4800 2100 4800
Wire Wire Line
	2100 4800 2100 5600
Wire Wire Line
	2100 5600 2400 5600
Connection ~ 2100 5600
Wire Wire Line
	2100 5600 2100 6200
Wire Wire Line
	2100 6200 2400 6200
Connection ~ 2100 6200
Wire Wire Line
	2100 6200 2100 6650
$Comp
L power:GND #PWR?
U 1 1 5E6D8DBB
P 2100 7000
F 0 "#PWR?" H 2100 6750 50  0001 C CNN
F 1 "GND" H 2105 6827 50  0000 C CNN
F 2 "" H 2100 7000 50  0001 C CNN
F 3 "" H 2100 7000 50  0001 C CNN
	1    2100 7000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E6D90DE
P 2100 6800
F 0 "C?" H 2215 6846 50  0000 L CNN
F 1 "0.1uF" H 2215 6755 50  0000 L CNN
F 2 "" H 2138 6650 50  0001 C CNN
F 3 "~" H 2100 6800 50  0001 C CNN
F 4 "1276-1006-1-ND" H 2100 6800 50  0001 C CNN "digikey"
	1    2100 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 6950 2100 7000
$Comp
L power:GND #PWR?
U 1 1 5E6D9DD9
P 3100 7200
F 0 "#PWR?" H 3100 6950 50  0001 C CNN
F 1 "GND" H 3105 7027 50  0000 C CNN
F 2 "" H 3100 7200 50  0001 C CNN
F 3 "" H 3100 7200 50  0001 C CNN
	1    3100 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 7100 3000 7200
Wire Wire Line
	3000 7200 3100 7200
Wire Wire Line
	3200 7200 3200 7100
Connection ~ 3100 7200
Wire Wire Line
	3100 7200 3200 7200
$Comp
L CustomAndImportedParts:10118192-0001LF J?
U 1 1 5E6DBEE6
P 850 6100
F 0 "J?" H 792 6665 50  0000 C CNN
F 1 "10118192-0001LF" H 792 6574 50  0000 C CNN
F 2 "AMPHENOL_10118192-0001LF" H 850 6100 50  0001 L BNN
F 3 "Amphenol ICC" H 850 6100 50  0001 L BNN
F 4 "None" H 850 6100 50  0001 L BNN "Field4"
F 5 "Single Port 5 Contact Horizontal Right Angle Shielded Micro B USB 2.0 Connector" H 850 6100 50  0001 L BNN "Field5"
F 6 "609-4613-1-ND" H 850 6100 50  0001 L BNN "Field6"
F 7 "10118192-0001LF" H 850 6100 50  0001 L BNN "Field7"
F 8 "https://www.digikey.com/product-detail/en/amphenol-icc-fci/10118192-0001LF/609-4613-1-ND/2785378?utm_source=snapeda&utm_medium=aggregator&utm_campaign=symbol" H 850 6100 50  0001 L BNN "Field8"
	1    850  6100
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E6DE9FE
P 1250 6450
F 0 "#PWR?" H 1250 6200 50  0001 C CNN
F 1 "GND" H 1255 6277 50  0000 C CNN
F 2 "" H 1250 6450 50  0001 C CNN
F 3 "" H 1250 6450 50  0001 C CNN
	1    1250 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 6400 1250 6400
Wire Wire Line
	1250 6400 1250 6450
Wire Wire Line
	1150 6200 1250 6200
Wire Wire Line
	1250 6200 1250 6400
Connection ~ 1250 6400
$Comp
L Device:R R?
U 1 1 5E6E1024
P 1750 5900
F 0 "R?" V 1957 5900 50  0000 C CNN
F 1 "27" V 1866 5900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1680 5900 50  0001 C CNN
F 3 "~" H 1750 5900 50  0001 C CNN
F 4 "738-RMCF0805JT27R0CT-ND" V 1750 5900 50  0001 C CNN "digikey"
	1    1750 5900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6C4B82
P 1750 6000
F 0 "R?" V 1650 6000 50  0000 C CNN
F 1 "27" V 1550 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1680 6000 50  0001 C CNN
F 3 "~" H 1750 6000 50  0001 C CNN
F 4 "738-RMCF0805JT27R0CT-ND" V 1750 6000 50  0001 C CNN "digikey"
	1    1750 6000
	0    -1   -1   0   
$EndComp
Text Label 1450 5800 2    50   ~ 0
VBUS
NoConn ~ 1150 6100
Wire Wire Line
	1150 5800 1450 5800
Wire Wire Line
	1150 5900 1600 5900
Wire Wire Line
	1150 6000 1600 6000
Wire Wire Line
	1900 5900 2400 5900
Wire Wire Line
	1900 6000 2400 6000
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 5E6C83C3
P 5350 6500
F 0 "Q?" H 5541 6546 50  0000 L CNN
F 1 "MMBT3904" H 5541 6455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5550 6600 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/MMBT3904.pdf" H 5350 6500 50  0001 C CNN
F 4 "1727-4044-1-ND" H 5350 6500 50  0001 C CNN "digikey"
	1    5350 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q?
U 1 1 5E6C84A7
P 5350 7200
F 0 "Q?" H 5541 7154 50  0000 L CNN
F 1 "MMBT3904" H 5541 7245 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5550 7300 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/MMBT3904.pdf" H 5350 7200 50  0001 C CNN
F 4 "1727-4044-1-ND" H 5350 7200 50  0001 C CNN "digikey"
	1    5350 7200
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6C8941
P 4950 6500
F 0 "R?" V 5050 6500 50  0000 L CNN
F 1 "10k" V 5150 6500 50  0000 L CNN
F 2 "" V 4880 6500 50  0001 C CNN
F 3 "~" H 4950 6500 50  0001 C CNN
F 4 "311-10.0KCRCT-ND" H 4950 6500 50  0001 C CNN "digikey"
	1    4950 6500
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6C91F4
P 4950 7200
F 0 "R?" V 5050 7200 50  0000 L CNN
F 1 "10k" V 5150 7200 50  0000 L CNN
F 2 "" V 4880 7200 50  0001 C CNN
F 3 "~" H 4950 7200 50  0001 C CNN
F 4 "311-10.0KCRCT-ND" H 4950 7200 50  0001 C CNN "digikey"
	1    4950 7200
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 6300 5450 6200
Wire Wire Line
	5450 6200 5900 6200
Wire Wire Line
	5450 7400 5450 7500
Wire Wire Line
	5450 7500 5900 7500
Text Label 4500 6500 0    50   ~ 0
RTS
Text Label 4500 7200 0    50   ~ 0
DTR
Text Label 5900 7500 2    50   ~ 0
EN
Text Label 5900 6200 2    50   ~ 0
IO0
Wire Wire Line
	5450 7000 5450 6950
Wire Wire Line
	5450 6950 5200 6750
Wire Wire Line
	5200 6750 4700 6750
Wire Wire Line
	5450 6700 5450 6750
Wire Wire Line
	5450 6750 5200 6950
Wire Wire Line
	5200 6950 4700 6950
Wire Wire Line
	4500 6500 4700 6500
Wire Wire Line
	4500 7200 4700 7200
Wire Wire Line
	4700 6750 4700 6500
Connection ~ 4700 6500
Wire Wire Line
	4700 6500 4800 6500
Wire Wire Line
	4700 6950 4700 7200
Connection ~ 4700 7200
Wire Wire Line
	4700 7200 4800 7200
Wire Wire Line
	5100 6500 5150 6500
Wire Wire Line
	5100 7200 5150 7200
Text Label 4100 5600 2    50   ~ 0
TXD
Text Label 4100 5700 2    50   ~ 0
RXD
Text Label 4100 5800 2    50   ~ 0
RTS
Text Label 4100 6000 2    50   ~ 0
DTR
Wire Wire Line
	3800 5600 4100 5600
Wire Wire Line
	3800 5700 4100 5700
Wire Wire Line
	3800 5800 4100 5800
Wire Wire Line
	3800 6000 4100 6000
NoConn ~ 3800 5900
NoConn ~ 3800 6100
NoConn ~ 3800 6200
NoConn ~ 3800 6300
NoConn ~ 1300 2400
NoConn ~ 1300 2500
NoConn ~ 1300 2600
NoConn ~ 1300 2700
NoConn ~ 1300 2800
NoConn ~ 1300 2900
$Comp
L Device:R R?
U 1 1 5E6E7602
P 5400 5200
F 0 "R?" V 5193 5200 50  0000 C CNN
F 1 "0" V 5284 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5330 5200 50  0001 C CNN
F 3 "~" H 5400 5200 50  0001 C CNN
F 4 "RMCF0805ZT0R00CT-ND" V 5400 5200 50  0001 C CNN "digikey"
	1    5400 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E6E7B1F
P 5400 5400
F 0 "R?" V 5500 5400 50  0000 C CNN
F 1 "0" V 5600 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5330 5400 50  0001 C CNN
F 3 "~" H 5400 5400 50  0001 C CNN
F 4 "RMCF0805ZT0R00CT-ND" V 5400 5400 50  0001 C CNN "digikey"
	1    5400 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	4900 5200 5250 5200
Wire Wire Line
	4900 5400 5250 5400
Wire Wire Line
	5550 5400 5900 5400
Wire Wire Line
	5550 5200 5900 5200
Text Label 4900 5200 0    50   ~ 0
RXD
Text Label 4900 5400 0    50   ~ 0
TXD
Text Label 5900 5200 2    50   ~ 0
TXD0
Text Label 5900 5400 2    50   ~ 0
RXD0
Text Label 2900 1300 2    50   ~ 0
RXD0
Text Label 2900 1500 2    50   ~ 0
TXD0
Wire Bus Line
	550  4700 6000 4700
Wire Bus Line
	6000 4700 6000 7600
Wire Bus Line
	6000 7600 550  7600
Wire Bus Line
	550  7600 550  4700
Text Notes 600  4800 0    50   ~ 0
USB-UART Communication\n
NoConn ~ 2500 1400
NoConn ~ 2500 1800
NoConn ~ 2500 2100
$Comp
L Device:LED D?
U 1 1 5E711051
P 3050 4050
F 0 "D?" V 3088 3932 50  0000 R CNN
F 1 "LED GREEN" V 2997 3932 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3050 4050 50  0001 C CNN
F 3 "~" H 3050 4050 50  0001 C CNN
F 4 "732-4983-1-ND" V 3050 4050 50  0001 C CNN "digikey"
	1    3050 4050
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5E71480C
P 4450 5700
F 0 "D?" V 4488 5582 50  0000 R CNN
F 1 "LED RED" V 4397 5582 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4450 5700 50  0001 C CNN
F 3 "~" H 4450 5700 50  0001 C CNN
F 4 "732-4984-1-ND" V 4450 5700 50  0001 C CNN "digikey"
	1    4450 5700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3800 6500 4250 6500
Text Label 4250 6500 2    50   ~ 0
TXRX
Text Label 4650 6100 2    50   ~ 0
TXRX
Wire Wire Line
	4450 5850 4450 6100
Wire Wire Line
	4450 6100 4650 6100
$Comp
L power:+3.3V #PWR?
U 1 1 5E717C2F
P 4450 5450
F 0 "#PWR?" H 4450 5300 50  0001 C CNN
F 1 "+3.3V" H 4465 5623 50  0000 C CNN
F 2 "" H 4450 5450 50  0001 C CNN
F 3 "" H 4450 5450 50  0001 C CNN
	1    4450 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 5450 4450 5550
Text Notes 550  7550 0    50   ~ 0
CBUS0 needs to be configured to TX&RXLED# using the FT_PROG utility\n
$Comp
L power:GND #PWR?
U 1 1 5E719EFA
P 3050 4300
F 0 "#PWR?" H 3050 4050 50  0001 C CNN
F 1 "GND" H 3055 4127 50  0000 C CNN
F 2 "" H 3050 4300 50  0001 C CNN
F 3 "" H 3050 4300 50  0001 C CNN
	1    3050 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4300 3050 4200
Wire Wire Line
	3050 3900 3050 3750
Text Label 3400 3750 2    50   ~ 0
IND_LED
Wire Wire Line
	3050 3750 3400 3750
Text Label 2900 1600 2    50   ~ 0
IND_LED
Wire Wire Line
	2500 1200 2900 1200
Wire Wire Line
	2500 1300 2900 1300
Wire Wire Line
	2500 1500 2900 1500
Wire Wire Line
	2500 1600 2900 1600
$EndSCHEMATC
